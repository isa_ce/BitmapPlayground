﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02_BitmapPlayground.Filters
{
    /// <summary>
    /// Filters the red component from an image.
    /// </summary>
    public class MovingAverageFilter : IFilter
    {
        public Color[,] Apply(Color[,] input)
        {
            int width = input.GetLength(0);
            int height = input.GetLength(1);
            Color[,] result = new Color[width, height];

            for (int x = 1; x < width - 1; x++)
            {
                for (int y = 1; y < height - 1; y++)
                {
                    var p = input[x, y];
                    var pleft = input[(x - 1), y];
                    var pright = input[x, (y - 1)];
                    var pabove = input[(x + 1), y];
                    var pbelow = input[x, (y + 1)];
                    int avgr = (pleft.R + pright.R + pabove.R + pbelow.R) / 4;
                    int avgg = (pleft.G + pright.G + pabove.G + pbelow.G) / 4;
                    int avgb = (pleft.B + pright.B + pabove.B + pbelow.B) / 4;
                    result[x, y] = Color.FromArgb(p.A, avgr, avgg, avgb);
                }
            }


            return result;
        }

        public string Name => "Moving Average Filter";

        public override string ToString()
            => Name;
    }
}
